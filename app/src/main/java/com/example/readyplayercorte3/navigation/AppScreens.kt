package com.example.recyclerviewactivity.navigation

import android.provider.ContactsContract.DisplayPhoto
import androidx.annotation.DrawableRes
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.painterResource
import com.example.readyplayercorte3.R

sealed class AppScreens(val route: String, val photo: Int){
    object HomeScreen: AppScreens("Home", R.drawable.wallpaper)
    object TechnologyScreen: AppScreens("Technology", R.drawable.computer)
    object GamesScreen: AppScreens("Games", R.drawable.games)
    object MoviesScreen: AppScreens("Movies", R.drawable.movies)
    object DetailScreen: AppScreens("detail/{id}",  R.drawable.detail){
        fun showDetail(id:Int)= "detail/$id"
    }
    object DevelopScreen: AppScreens("Develop", R.drawable.develop)
    object GridScreen: AppScreens("Grid", R.drawable.grid)
    object MusicScreen: AppScreens("Music", R.drawable.music)
    object PlacesScreen: AppScreens("Places", R.drawable.places)
}
