package com.example.recyclerviewactivity
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.readyplayercorte3.screens.DetailScreen
import com.example.readyplayercorte3.screens.DevelopScreen
import com.example.readyplayercorte3.screens.GamesScreen
import com.example.readyplayercorte3.screens.GridScreen
import com.example.readyplayercorte3.screens.HomeScreen
import com.example.readyplayercorte3.screens.MoviesScreen
import com.example.readyplayercorte3.screens.MusicScreen
import com.example.readyplayercorte3.screens.PlacesScreen
import com.example.readyplayercorte3.screens.TechnologyScreen
//import androidx.navigation.compose.NavHost
//import androidx.navigation.compose.composable
//import androidx.navigation.compose.rememberNavController
import com.example.recyclerviewactivity.navigation.AppScreens
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppNavigation(){
val navController= rememberNavController()
    NavHost(navController=navController, startDestination=AppScreens.HomeScreen.route) {// star indica a que pantalla se tiene que ir cuando arranque la app
        composable(route = AppScreens.HomeScreen.route) {
            HomeScreen(navController)
        }
        composable(route=AppScreens.TechnologyScreen.route){
            TechnologyScreen(navController)
        }
        composable(route=AppScreens.GamesScreen.route){
            GamesScreen(navController)
        }
        composable(route=AppScreens.MoviesScreen.route){
            MoviesScreen(navController)
        }
        composable(route=AppScreens.DetailScreen.route,
                arguments = listOf(navArgument("id") { type = NavType.IntType })
        ){
                navBackStackEntry -> navBackStackEntry.arguments?.let {
            DetailScreen(navController, it.getInt("id"))
        }
        }
        composable(route=AppScreens.DevelopScreen.route){
            DevelopScreen(navController)
        }
        composable(route=AppScreens.GridScreen.route){
            GridScreen(navController)
        }
        composable(route=AppScreens.MusicScreen.route){
            MusicScreen(navController)
        }
        composable(route=AppScreens.PlacesScreen.route){
            PlacesScreen(navController)
        }
    }
}