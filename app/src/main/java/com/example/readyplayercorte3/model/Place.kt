package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes

data class Place(val name: String, @DrawableRes val photo:Int)
