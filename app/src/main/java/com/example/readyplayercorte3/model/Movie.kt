package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes

data class Movie(val id: Int, val name: String, @DrawableRes val photo:Int)
