package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes

data class GridModel(@DrawableRes val photo:Int)
