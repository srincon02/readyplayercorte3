package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes

data class Game(val name: String, @DrawableRes val photo:Int)
