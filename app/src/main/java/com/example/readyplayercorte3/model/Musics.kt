package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes
import androidx.annotation.RawRes

data class Musics(val name: String,val author: String, @RawRes val song:Int)
