package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes

data class MovieDetails(val name: String, val age: Int, val sipnosis: String, @DrawableRes val photo:Int)
