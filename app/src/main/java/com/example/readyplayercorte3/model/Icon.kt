package com.example.readyplayercorte3.model

import androidx.annotation.DrawableRes
import com.example.recyclerviewactivity.navigation.AppScreens

data class Icon(val route: AppScreens, @DrawableRes val photo:Int)
