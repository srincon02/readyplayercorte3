package com.example.readyplayercorte3.screens
import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.readyplayercorte3.R
import com.example.readyplayercorte3.model.SMenu
import com.example.recyclerviewactivity.navigation.AppScreens
import kotlinx.coroutines.launch
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@ExperimentalMaterial3Api
@Composable
fun HomeScreen(navController: NavController) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val bungueweFontFamily = FontFamily(
        Font(
            resId = R.font.bungee,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        content = {
                Image(
                    painter = painterResource(id = R.drawable.wallpaper),
                    contentDescription = null,
                    modifier = Modifier.fillMaxSize(),
                    contentScale = ContentScale.Crop
                )
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()) {
                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    item{
                        Text(
                            text = "READY",
                            modifier = Modifier.fillMaxWidth().padding(top = 100.dp),
                            textAlign = TextAlign.Center,
                            fontFamily = bungueweFontFamily,
                            fontSize = 40.sp,
                        )
                        Text(
                            text = "PLAYER",
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.Center,
                            fontFamily = bungueweFontFamily,
                            fontSize = 60.sp,
                        )
                        Text(
                            text = "ONE",
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.Center,
                            fontFamily = bungueweFontFamily,
                            fontSize = 80.sp,
                        )
                    }
                    items(getScreens()) {
                        ScreenView(it, navController)
                    }

                }
            }

            },
        topBar = {
            TopAppBar(
                title = { Text(text = "Home", fontFamily = fascinateFontFamily) },
                actions = {
                    var isMenuOpen by remember {
                        mutableStateOf(false)
                    }
                    IconButton(onClick = { isMenuOpen = true }) {
                        Icon(
                            imageVector = Icons.Outlined.MoreVert,
                            contentDescription = null
                        )
                    }
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor =  MaterialTheme.colorScheme.secondary,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary
                )
            )
        },
    )
}
@Composable
fun ScreenView(screen: AppScreens, navController: NavController) {
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    Column {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource(id = screen.photo),
                contentDescription = null,
                modifier = Modifier
                    //.fillMaxWidth()
                    //.padding(16.dp)
                    .sizeIn(50.dp, 50.dp, 50.dp, 50.dp)
                    .clip(CircleShape),
            )
            TextButton(onClick={navController.navigate(route= screen.route)}){//solo un texto que funciona como boton pero no tiene propiedades del boton como colores
                Text(text = screen.route,
                    fontFamily = fascinateFontFamily,
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    color = Color.Black )
            }
        }
    }
}
fun getScreens(): List<AppScreens> {
    return listOf(
        AppScreens.TechnologyScreen,
        AppScreens.GamesScreen,
        AppScreens.MoviesScreen,
        AppScreens.MusicScreen,
        AppScreens.PlacesScreen,
    )}
@OptIn(ExperimentalMaterial3Api::class)
@Preview(showSystemUi = true, showBackground = true)
@Composable
fun HomeScreenPreview() {
    val navController = rememberNavController()
    HomeScreen(navController = navController)
}