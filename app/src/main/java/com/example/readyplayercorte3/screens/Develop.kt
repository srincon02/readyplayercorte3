package com.example.readyplayercorte3.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material.icons.twotone.Person
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.readyplayercorte3.R
import com.example.readyplayercorte3.model.SBottomBarImpar
import com.example.readyplayercorte3.model.SMenu
import com.example.recyclerviewactivity.navigation.AppScreens
import kotlinx.coroutines.launch
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@ExperimentalMaterial3Api
@Composable
fun DevelopScreen(navController: NavController) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        content = {
            Image(
                painter = painterResource(id = R.drawable.wallpaperscr),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize().padding(bottom= 100.dp)) {
                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    item{
                        Card(
                            modifier = Modifier.fillMaxWidth().padding(top= 70.dp),
                            border = BorderStroke(4.dp, color = Color.Black),
                            elevation = CardDefaults.cardElevation(defaultElevation = 8.dp)
                        ) {
                            Column(
                                modifier = Modifier
                                    .fillMaxSize()
                                    //.background(MaterialTheme.colorScheme.onPrimaryContainer)
                                    .background(color = Color(0xFF9E3641))
                            ) {
                                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                                    Image(
                                        painter = painterResource(id = R.drawable.coraline),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .height(400.dp)
                                            .width(300.dp)
                                            .padding(20.dp)
                                            .clip(RoundedCornerShape(16.dp))
                                            .border(
                                                width = 4.dp,
                                                color = Color.Black,
                                                shape = RoundedCornerShape(16.dp)
                                            )
                                            .background(Color.White)
                                    )
                                    Text(
                                        text = "Sarita Rincón Gutierrez",
                                        fontFamily = fascinateFontFamily,
                                        fontSize = 25.sp,
                                        textAlign = TextAlign.Center,
                                        color = MaterialTheme.colorScheme.onPrimary
                                    )
                                    Text(
                                        text = "Estudiante de ingenieria de sistemas y computación",
                                        fontFamily = fascinateFontFamily,
                                        fontSize = 20.sp,
                                        textAlign = TextAlign.Center,
                                        color = MaterialTheme.colorScheme.onPrimary
                                    )
                                    Text(
                                        text = "8vo semestre",
                                        fontFamily = fascinateFontFamily,
                                        fontSize = 15.sp,
                                        textAlign = TextAlign.Center,
                                        color = MaterialTheme.colorScheme.onPrimary
                                    )
                                    Spacer(modifier = Modifier.height(30.dp))
                                }
                            }
                        }
                    }

                }
            }
        },
        topBar = {
            TopAppBar(
                title = {
                    Column {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Image(
                                painter = painterResource(id = R.drawable.develop),
                                contentDescription = null,
                                modifier = Modifier
                                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                                    .clip(CircleShape),
                            )
                            Text(text = "Developer",
                                fontFamily = fascinateFontFamily,
                                fontSize = 16.sp,
                                color = MaterialTheme.colorScheme.onPrimary
                            )
                        }
                    } },
                actions = {
                    var isMenuOpen by remember {
                        mutableStateOf(false)
                    }
                    IconButton(onClick = { isMenuOpen = true }) {
                        Icon(
                            imageVector = Icons.Outlined.MoreVert,
                            contentDescription = null
                        )
                    }
                    SMenu(isMenuOpen, navController, itemClick = {
                        scope.launch {
                            snackbarHostState.showSnackbar(it)
                        }
                    }) { isMenuOpen = false }
                    IconButton(onClick = {navController.navigate(route= AppScreens.HomeScreen.route)}) {
                        Icon(
                            painter = painterResource(id = R.drawable.house),
                            contentDescription = null,
                            modifier = Modifier
                                .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                                .clip(CircleShape),
                        )
                    }

                },
                colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor =  MaterialTheme.colorScheme.onSecondaryContainer
                )
            )
        },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                text = { Text(text = "Category") },
                icon = { Icons.TwoTone.Person },
                onClick = {
                    scope.launch {
                        snackbarHostState.showSnackbar("Developer")
                    }
                })
        },
        bottomBar = { SBottomBarImpar(navController) }
    )
}