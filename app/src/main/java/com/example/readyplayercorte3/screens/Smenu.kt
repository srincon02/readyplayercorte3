package com.example.readyplayercorte3.model

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.readyplayercorte3.R
import com.example.recyclerviewactivity.navigation.AppScreens

@Composable
fun SMenu(
    isOpen: Boolean,
    navigationController: NavController,
    itemClick: (String) -> Unit,
    onClose: () -> Unit,
) {
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    val itemsMenu = listOf(
        AppScreens.DevelopScreen,
        AppScreens.GridScreen,
    )
    DropdownMenu(expanded = isOpen, onDismissRequest = { onClose() }) {
        itemsMenu.forEach {
            DropdownMenuItem(onClick = {
                navigationController.navigate(it.route)
                itemClick(it.route)
                onClose()
            },
                text = {
                    Column {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Image(
                                painter = painterResource(id = it.photo),
                                contentDescription = null,
                                modifier = Modifier
                                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                                    .clip(CircleShape),
                            )
                            Text(text = it.route,
                                fontFamily = fascinateFontFamily,
                                fontSize = 16.sp,
                                color = Color.Black,
                            )
                        }
                    }
                })
        }
    }
}
@Composable
fun SBottomBarPar(navController:NavController) {
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    BottomAppBar( modifier = Modifier.background( MaterialTheme.colorScheme.secondary)
    ) {
        var index by remember {
            mutableStateOf(0)

        }
        NavigationBarItem(
            selected = index==1,
            onClick = { navController.navigate(route= AppScreens.GamesScreen.route)},
            icon = {   Image(
                painter = painterResource(id = R.drawable.games),
                contentDescription = null,
                modifier = Modifier
                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                    .clip(CircleShape),
            )
                /*Icon(imageVector = Icons.TwoTone.Person, contentDescription = null)*/ },
            label = { Text(text = "Games", fontFamily = fascinateFontFamily,) }
        )
        NavigationBarItem(
            selected = index==1,
            onClick = { navController.navigate(route= AppScreens.MusicScreen.route)},
            icon = {   Image(
                painter = painterResource(id = R.drawable.music),
                contentDescription = null,
                modifier = Modifier
                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                    .clip(CircleShape),
            )},
            label = { Text(text = "Music", fontFamily = fascinateFontFamily,) }
        )
        NavigationBarItem(
            selected = index==1,
            onClick = { navController.navigate(route= AppScreens.GridScreen.route)},
            icon = {   Image(
                painter = painterResource(id = R.drawable.grid),
                contentDescription = null,
                modifier = Modifier
                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                    .clip(CircleShape),
            )
            },
            label = { Text(text = "Grid", fontFamily = fascinateFontFamily,) }
        )
    }
}
@Composable
fun SBottomBarImpar(navController:NavController) {
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    BottomAppBar( modifier = Modifier.background( MaterialTheme.colorScheme.secondary)
    ) {
        var index by remember {
            mutableStateOf(0)

        }
        NavigationBarItem(
            selected = index==1,
            onClick = { navController.navigate(route= AppScreens.TechnologyScreen.route)},
            icon = {   Image(
                painter = painterResource(id = R.drawable.computer),
                contentDescription = null,
                modifier = Modifier
                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                    .clip(CircleShape),
            )
                /*Icon(imageVector = Icons.TwoTone.Person, contentDescription = null)*/ },
            label = { Text(text = "Technology", fontFamily = fascinateFontFamily,) }
        )
        NavigationBarItem(
            selected = index==1,
            onClick = { navController.navigate(route= AppScreens.MoviesScreen.route)},
            icon = {   Image(
                painter = painterResource(id = R.drawable.movies),
                contentDescription = null,
                modifier = Modifier
                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                    .clip(CircleShape),
            )
                /*Icon(imageVector = Icons.TwoTone.Person, contentDescription = null)*/ },
            label = { Text(text = "Movies", fontFamily = fascinateFontFamily,) }
        )
        NavigationBarItem(
            selected = index==1,
            onClick = { navController.navigate(route= AppScreens.PlacesScreen.route)},
            icon = {   Image(
                painter = painterResource(id = R.drawable.places),
                contentDescription = null,
                modifier = Modifier
                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                    .clip(CircleShape),
            )
            },
            label = { Text(text = "Places", fontFamily = fascinateFontFamily,) }
        )
    }
}
