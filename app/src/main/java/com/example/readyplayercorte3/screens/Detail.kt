package com.example.readyplayercorte3.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material.icons.twotone.Person
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.readyplayercorte3.R
import com.example.readyplayercorte3.model.Movie
import com.example.readyplayercorte3.model.MovieDetails
import com.example.readyplayercorte3.model.SBottomBarPar
import com.example.readyplayercorte3.model.SMenu
import com.example.recyclerviewactivity.navigation.AppScreens
import kotlinx.coroutines.launch
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@ExperimentalMaterial3Api
@Composable
fun DetailScreen(navController: NavController, movieId:Int) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        content = {
            Image(
                painter = painterResource(id = R.drawable.wallpaperscr),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize().padding(bottom= 100.dp)) {
                        MovieDetails(navController,movieId)
            }
        },
        topBar = {
            TopAppBar(
                title = {
                    Column {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Image(
                                painter = painterResource(id = R.drawable.movies),
                                contentDescription = null,
                                modifier = Modifier
                                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                                    .clip(CircleShape),
                            )
                            Text(text = "Movies",
                                fontFamily = fascinateFontFamily,
                                fontSize = 16.sp,
                                color = MaterialTheme.colorScheme.onPrimary
                            )
                        }
                    } },
                actions = {
                    var isMenuOpen by remember {
                        mutableStateOf(false)
                    }
                    IconButton(onClick = { isMenuOpen = true }) {
                        Icon(
                            imageVector = Icons.Outlined.MoreVert,
                            contentDescription = null
                        )
                    }
                    SMenu(isMenuOpen, navController, itemClick = {
                        scope.launch {
                            snackbarHostState.showSnackbar(it)
                        }
                    }) { isMenuOpen = false }
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor =  MaterialTheme.colorScheme.onSecondaryContainer
                )
            )
        },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                text = { Text(text = "Home") },
                icon = { Icons.TwoTone.Person },
                onClick = {
                    navController.navigate(route= AppScreens.HomeScreen.route)
                })
        },
        bottomBar = { SBottomBarPar(navController) }
    )
}
@Composable
fun MovieDetails(navController: NavController, movieId:Int) {
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    Card(
        modifier = Modifier.fillMaxWidth().padding(top = 70.dp),
        border = BorderStroke(4.dp, color = Color.Black),
        elevation = CardDefaults.cardElevation(defaultElevation = 8.dp)
    ) {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            item {
                Image(
                    painter = painterResource(id = getMovieDetails()[movieId].photo),
                    contentDescription = null,
                    modifier = Modifier
                        .height(550.dp)
                        .fillMaxWidth()
                        .padding(20.dp)
                        .clip(RoundedCornerShape(16.dp))
                        .border(
                            width = 4.dp,
                            color = Color.Black,
                            shape = RoundedCornerShape(16.dp)
                        )
                        .background(Color.White)
                )
                Spacer(modifier = Modifier.height(4.dp))
                Column(modifier = Modifier.padding(horizontal = 20.dp), horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(
                        text = getMovieDetails()[movieId].name,
                        fontFamily = fascinateFontFamily,
                        fontSize = 30.sp,
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                    Text(
                        text = getMovieDetails()[movieId].age.toString(),
                        fontFamily = fascinateFontFamily,
                        fontSize = 20.sp,
                        textAlign = TextAlign.Center,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                    Spacer(modifier = Modifier.height(20.dp))
                    Text(modifier= Modifier
                        .fillMaxWidth()
                        ,text = getMovieDetails()[movieId].sipnosis,
                        fontWeight = FontWeight.Normal,
                        fontFamily = fascinateFontFamily,
                        fontSize = 18.sp,
                        color = Color.Black,
                        textAlign = TextAlign.Justify,
                    )
                    Spacer(modifier = Modifier.height(30.dp))
                }
            }
        }
    }
}
fun getMovieDetails(): List<MovieDetails> {
    return listOf(
        MovieDetails("Howard un nuevo heroe", 1986, "Howard es un pato que vive apaciblemente en su planeta habitado también por patos y del que es arrebatado un día para dar con sus plumas en el planeta Tierra a consecuencia de un experimento científico. En medio de Cleveland, en el estado de Ohio, Howard luchará hasta lo indecible para no ser atrapado.", R.drawable.howard_p),
        MovieDetails("Lady Halcon",1985,"Edad Media. Una leyenda de carácter sobrenatural relata la diabólica venganza del Obispo de Aquila, que consiste en hacer imposible el amor entre Navarre (Rutger Hauer) e Isabeau Anjou (Michelle Pfeiffer). Aliándose con las fuerzas del mal, el Obispo consigue hechizar a los amantes: ella se convertirá en halcón durante el día, y él será un lobo por la noche.", R.drawable.lady_p),
        MovieDetails("Krull",1983,"El planeta Krull es atacado por un monstruo y un ejército de alienígenas asesinos. Para hacerles frente, dos naciones enemigas deciden unir sus fuerzas por medio del matrimonio del príncipe Colwyn y la princesa Lyssa; pero el día de la boda, el palacio es asaltado, la princesa secuestrada y Colwyn herido. El príncipe deberá entonces encontrar una espada voladora para rescatar a la princesa y librar al planeta de tan terrible amenaza.", R.drawable.krull_p),
        MovieDetails("Los cazafantasmas",1984,"A los doctores Venkman, Stantz y Spengler, expertos en parapsicología, no les conceden una beca de investigación que habían solicitado. Al encontrarse sin trabajo, deciden fundar la empresa \"Los Cazafantasmas\", dedicada a limpiar Nueva York de ectoplasmas. El aumento repentino de apariciones espectrales en la ciudad será el presagio de la llegada de un peligroso y poderoso demonio.", R.drawable.cazafantasmas_p),
        MovieDetails("Escuela de genios",1985,"Mitch Taylor es un chico de quince años que gracias a sus experimentos con láser ultravioleta ha conseguido una beca en un centro experimental en Pacific Teach. A pesar de ser un genio está asustado de vivir lejos de su casa. Chris Knight hace tres años que estudia en el centro y está considerado como uno de los mejores, como uno de los cerebros juveniles del país.", R.drawable.escuela_de_genios_p),
        MovieDetails("Más vale muerto",1985,"Una vez que su novia le abandona por un esquiador palurdo, Lane decide que el suicidio es su única salida. Pero sus intentonas serán cada vez más fallidas, trayéndole sólo más angustia y desconcierto.", R.drawable.mas_vale_muerto_p),
        MovieDetails("La revancha de los novatos",1984,"Lewis y Gilbert comienzan la universidad ilusionados, creyendo que va a ser una etapa muy diferente a la del instituto, pero una vez llegados a la universidad Adams, se encuentran con que no son más que unos \"nerds\" objeto de burlas y se quedan sin alojamiento porque los estudiantes del equipo universitario tienen prioridad. Forzados a dormir en el gimnasio con el resto de \"nerds\", todos hacen piña e intentan fundar su propia fraternidad y defenderse de los ataques de las fraternidades de los deportistas y populares.", R.drawable.revenge_of_the_nerds_p),
        MovieDetails("El señor de los anillos",2001,"En la Tierra Media, el Señor Oscuro Sauron ordenó a los Elfos que forjaran los Grandes Anillos de Poder. Tres para los reyes Elfos, siete para los Señores Enanos, y nueve para los Hombres Mortales. Pero Saurón también forjó, en secreto, el Anillo Único, que tiene el poder de esclavizar toda la Tierra Media. Con la ayuda de sus amigos y de valientes aliados, el joven hobbit Frodo emprende un peligroso viaje con la misión de destruir el Anillo Único.", R.drawable.sr_anillos_p),
        MovieDetails("Juegos de guerra",1983,"David es un experto informático capaz de saltarse los más avanzados sistemas de seguridad y de descifrar los más herméticos códigos secretos. Pero su juego se complica cuando involuntariamente conecta su ordenador al del Departamento de Defensa americano, encargado del sistema de defensa nuclear. Desencadena así una situación de peligro difícilmente controlable. Con la ayuda de su novia y de otro informático genial intentará, en una carrera contrarreloj, evitar la Tercera Guerra Mundial.", R.drawable.juegosdeguerra_p),
        MovieDetails("Star wars",1977, "Su trama describe las vivencias de un grupo de personajes que habitan en una galaxia ficticia e interactúan con elementos como «la Fuerza», un campo de energía metafísico y omnipresente10 que posee un «lado luminoso» impulsado por la sabiduría, la nobleza y la justicia y utilizado por los Jedi, y un «lado oscuro» usado por los Sith y provocado por la ira, el miedo, el odio y la desesperación.", R.drawable.star_wars_p),
    )}

