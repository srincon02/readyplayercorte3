package com.example.readyplayercorte3.screens

import android.annotation.SuppressLint
import android.media.MediaPlayer
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material.icons.twotone.Person
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.readyplayercorte3.R
import com.example.readyplayercorte3.model.Musics
import com.example.readyplayercorte3.model.SBottomBarImpar
import com.example.readyplayercorte3.model.SMenu
import com.example.recyclerviewactivity.navigation.AppScreens
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@ExperimentalMaterial3Api
@Composable
fun MusicScreen(navController: NavController) {

    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    Scaffold(
        snackbarHost = { SnackbarHost(snackbarHostState) },
        content = {
            Image(
                painter = painterResource(id = R.drawable.wallpaperscr),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )
            Column(verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize().padding(bottom= 100.dp)) {
                LazyColumn(
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    items(getMusic()) {
                        Musics(it)
                    }

                }
            }
        },
        topBar = {
            TopAppBar(
                title = {
                    Column {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Image(
                                painter = painterResource(id = R.drawable.music),
                                contentDescription = null,
                                modifier = Modifier
                                    .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                                    .clip(CircleShape),
                            )
                            Text(text = "Music",
                                fontFamily = fascinateFontFamily,
                                fontSize = 16.sp,
                                color = MaterialTheme.colorScheme.onPrimary
                            )
                        }
                    } },
                actions = {
                    var isMenuOpen by remember {
                        mutableStateOf(false)
                    }
                    IconButton(onClick = { isMenuOpen = true }) {
                        Icon(
                            imageVector = Icons.Outlined.MoreVert,
                            contentDescription = null
                        )
                    }
                    SMenu(isMenuOpen, navController, itemClick = {
                        scope.launch {
                            snackbarHostState.showSnackbar(it)
                        }
                    }) { isMenuOpen = false }
                    IconButton(onClick = {navController.navigate(route= AppScreens.HomeScreen.route)}) {
                        Icon(
                            painter = painterResource(id = R.drawable.house),
                            contentDescription = null,
                            modifier = Modifier
                                .sizeIn(30.dp, 30.dp, 30.dp, 30.dp)
                                .clip(CircleShape),
                        )
                    }

                },
                colors = TopAppBarDefaults.smallTopAppBarColors(
                    containerColor =  MaterialTheme.colorScheme.onSecondaryContainer
                )
            )
        },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                text = { Text(text = "Category") },
                icon = { Icons.TwoTone.Person },
                onClick = {
                    scope.launch {
                        snackbarHostState.showSnackbar("Music")
                    }
                })
        },
        bottomBar = { SBottomBarImpar(navController) }
    )
}
@Composable
fun Musics(music: Musics) {
    val fascinateFontFamily = FontFamily(
        Font(
            resId = R.font.fascinate,
            weight = FontWeight.ExtraBold,
            style = FontStyle.Normal
        )
    )
    val context = LocalContext.current
    val mediaPlayer = remember { MediaPlayer.create(context, music.song) }
    var isPlaying by remember { mutableStateOf(false) }
    Card(
        modifier = Modifier.width(300.dp).padding(top= 70.dp),
        border = BorderStroke(4.dp, color = Color.Black),
        elevation = CardDefaults.cardElevation(defaultElevation = 8.dp)
    ) {
        Column(
            modifier = Modifier
                .width(300.dp).fillMaxHeight()
                .background(color = Color(0xFFEC5F6E))
        ){
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                    painter = painterResource(R.drawable.disco_vinilo),
                    contentDescription = null,
                    modifier = Modifier
                        .height(300.dp)
                        .width(300.dp)
                        .padding(start=16.dp,end= 16.dp)
                )
                Text(
                    text = music.name,
                    fontFamily = fascinateFontFamily,
                    fontSize = 40.sp,
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.onPrimary
                )
                Text(
                    text = music.author,
                    fontFamily = fascinateFontFamily,
                    fontSize = 20.sp,
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colorScheme.onSecondaryContainer
                )
                Button(
                    onClick = {
                        if (isPlaying) {
                            mediaPlayer.pause()
                        } else {
                            mediaPlayer.start()
                        }
                        isPlaying = !isPlaying
                    },
                    modifier = Modifier.padding(16.dp)
                ) {
                    Icon(
                        imageVector = if (isPlaying) Icons.Filled.Close else Icons.Filled.PlayArrow,
                        contentDescription = if (isPlaying) "Detener" else "Reproducir",
                        tint = Color.White, modifier = Modifier.width(50.dp).height(50.dp)
                    )
                }
            }
        }
    }

}
fun getMusic(): List<Musics> {
    return listOf(
        Musics("Dead Man´s Party", "Oingo Boingo", R.raw.dead_mans_party),
        Musics("The Wild Boys", "Duran Duran", R.raw.the_wild_boys),
        Musics("Bird House In Your Soul", "They Might Be Giants", R.raw.they_might_be_giants),
        Musics("New Order", "Blue Monday", R.raw.newr_oder),
        Musics("James Brown Is Dead", "L.A Style", R.raw.james_brown_is_dead),
        Musics("Atomic", "Blondie", R.raw.atomic),
        Musics("Pour Some Sugar On Me", "Def Leppard", R.raw.pour_some_sugar_on_me),
    )}
